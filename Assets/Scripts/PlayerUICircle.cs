using UnityEngine;
using UnityEngine.UI;

public class PlayerUICircle : MonoBehaviour
{
    public RectTransform RectTransform => rectTransform;

    private CanvasScaler  canvasScaler;
    private RectTransform rectTransform;
    private Vector2       startSizeDelta;
    
    private void Awake()
    {
        rectTransform = transform.GetComponent<RectTransform>();
        canvasScaler = transform.GetComponentInParent<CanvasScaler>();
        
        startSizeDelta = rectTransform.sizeDelta;
    }
    
    public float GetDiameterInUnit() => rectTransform.rect.size.x / canvasScaler.referencePixelsPerUnit;

    public void ResetSize() => rectTransform.sizeDelta = startSizeDelta;
}
