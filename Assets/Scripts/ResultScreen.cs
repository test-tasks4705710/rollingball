using System;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreen : MonoBehaviour
{
    public static event Action OnRestart;
    
    [SerializeField] private Text   txtInfo;
    [SerializeField] private Button btnRestart;

    private void Start()
    {
        btnRestart.onClick.AddListener(OnRestartHandler);
    }

    public void Show(bool іsWon)
    {
        gameObject.SetActive(true);
        txtInfo.text = іsWon ? "You Won!" : "Game Over!";
    }

    private void OnRestartHandler()
    {
        gameObject.SetActive(false);
        OnRestart?.Invoke();
    }
}
