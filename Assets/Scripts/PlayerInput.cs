using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public enum ButtonState
    {
        PressDown,
        Hold,
        PressUp
    }
    
    public delegate void ButtonHandler(ButtonState buttonState);
    
    public event ButtonHandler OnInput;

    public bool IsActive { get; set; } = true;

    private void Update()
    {
        if (IsActive)
            ExecuteButtonHandle(0, OnInput);
    }

    private void ExecuteButtonHandle(int buttonId, ButtonHandler handler)
    {
        if (Input.GetMouseButtonDown(buttonId))
        {
            handler?.Invoke(ButtonState.PressDown);
        }
        else if (Input.GetMouseButtonUp(buttonId))
        {
            handler?.Invoke(ButtonState.PressUp);
        }
        else if (Input.GetMouseButton(buttonId))
        {
            handler?.Invoke(ButtonState.Hold);
        }
    }

}

