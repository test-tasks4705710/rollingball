using System;
using System.Collections;
using UnityEngine;
using static PlayerInput;

public class GameCore : MonoBehaviour
{
    public static event Action OnClearScene;
    
    [SerializeField] private ResultScreen    resultScreen;
    [SerializeField] private PlayerUICircle  uiBaseCircle;
    [SerializeField] private PlayerUICircle  uiAttackCircle;
    [SerializeField] private PlayerInput     input;
    [SerializeField] private PlayerSphere    sphere;
    [SerializeField] private Trajectory      trajectory;
    [SerializeField] private Door            door;
    [SerializeField] private Vector2         sizeDeltaModifier;
    [SerializeField] private float           distanceToOpenDoor;

    private bool sphereIsLaunched;
    private bool inputHandlerIsActive = true;
    
    private void Start()
    {
        door.OnSphereEnterDoor = () => { ShowResultScreen(true); };
        sphere.OnSphereHitObstacle = () => Invoke(nameof(NextTry), 2f);
            
        trajectory.SetWidth(CalculateSize(uiBaseCircle));
        trajectory.Calculate();
        sphere.TakeStartingPosition();
        
        Subscribe();
    }

    private void Subscribe()
    {
        ResultScreen.OnRestart += OnRestartHandler;
        input.OnInput += OnInputHandler;
    }

    private void Update()
    {
        if (sphereIsLaunched && !door.IsOpen)
        {
            var distance = Vector3.Distance(sphere.transform.position, door.transform.position);
            if (distance <= distanceToOpenDoor)
                door.Open();
        }
    }

    private void OnInputHandler(ButtonState buttonState)
    {
        if (inputHandlerIsActive)
        {
            switch (buttonState)
            {
                case ButtonState.PressDown:
                case ButtonState.Hold:
                {
                    uiBaseCircle.RectTransform.sizeDelta -= sizeDeltaModifier;
                    uiAttackCircle.RectTransform.sizeDelta += sizeDeltaModifier;
                    trajectory.SetWidth(CalculateSize(uiBaseCircle));

                    if (uiBaseCircle.RectTransform.sizeDelta.x <= uiAttackCircle.RectTransform.sizeDelta.x)
                        ShowResultScreen(false);
                    break;
                }
                case ButtonState.PressUp:
                {
                    ChangeEnabledStateOfUI(false);

                    sphereIsLaunched = true;
                    sphere.ChangeSize(CalculateSize(uiAttackCircle));
                    sphere.ChangeRendererEnabledState(true);
                    sphere.TakeShot();
                    uiAttackCircle.ResetSize();
                    break;
                }
            }
        }
    }

    private void NextTry()
    {
        ChangeEnabledStateOfUI(true);
        
        sphereIsLaunched = false;
        sphere.ChangeRendererEnabledState(false);
        sphere.TakeStartingPosition();
        
        OnClearScene?.Invoke();
    }
    
    private float CalculateSize(PlayerUICircle uiCircle)
    {
        const int percentSizeModifier = 60;
        return (uiCircle.GetDiameterInUnit() * percentSizeModifier) / 100;
    }

    private void ShowResultScreen(bool result)
    {
        ChangeEnabledStateOfUI(false);
        sphereIsLaunched = false;
        resultScreen.Show(result);
    }
    
    private void ChangeEnabledStateOfUI(bool state)
    {
        inputHandlerIsActive = state;
        uiBaseCircle.RectTransform.gameObject.SetActive(state);
        uiAttackCircle.RectTransform.gameObject.SetActive(state);
    }
    
    private void OnRestartHandler()
    {
        sphere.TakeStartingPosition();
        sphere.ResetPhysicalForces();
        uiBaseCircle.ResetSize();
        uiAttackCircle.ResetSize();
        trajectory.SetWidth(CalculateSize(uiBaseCircle));
        door.Close();
        StartCoroutine(SkipOneFrame(()=>ChangeEnabledStateOfUI(true)));//todo temporary solution, it is necessary to separate the game and ui EventSystem
    }

    private IEnumerator SkipOneFrame(Action action)
    {
        yield return new WaitForEndOfFrame();
        action?.Invoke();
    }
}
