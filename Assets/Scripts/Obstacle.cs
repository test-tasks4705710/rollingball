using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Renderer))]
public class Obstacle : MonoBehaviour
{
    private Rigidbody  rigidbody;
    private Renderer   renderer;
    private Vector3    startPosition;
    private Quaternion startRotation;
    private Color      startColor;
    private bool       isDeactivated;
    
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        renderer =  GetComponent<Renderer>();

        startRotation = transform.rotation;
        startPosition = transform.position;
        startColor = renderer.material.color;

        Subscribe();
    }

    private void Subscribe()
    {
        GameCore.OnClearScene += OnClearSceneHandler;
        ResultScreen.OnRestart += OnRestartHandler;
    }

    public void Explode(Vector3 position, float force, float radius)
    {
        isDeactivated = true;
        rigidbody.AddExplosionForce(force, position, radius);
    }

    public void ChangeColor(Color color) => renderer.material.SetColor("_Color", color);

    private void OnClearSceneHandler()
    {
        if (isDeactivated)
            gameObject.SetActive(false);
    }

    private void OnRestartHandler()
    {
        rigidbody.angularVelocity = Vector3.zero;
        rigidbody.velocity = Vector3.zero;
        gameObject.SetActive(true);
        isDeactivated = false;
        transform.position = startPosition;
        transform.rotation = startRotation;
        ChangeColor(startColor);
    }
}
