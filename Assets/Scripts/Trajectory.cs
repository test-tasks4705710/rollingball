using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Trajectory : MonoBehaviour
{
    [SerializeField] private Transform     door;
    [SerializeField] private RectTransform uiBaseCircle;
    
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = transform.GetComponent<SpriteRenderer>();
    }
    
    public void Calculate()
    {
        Ray ray = Camera.main.ScreenPointToRay(uiBaseCircle.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            transform.position = new Vector3(hit.point.x, transform.position.y, hit.point.z);
            spriteRenderer.size = new Vector2(spriteRenderer.size.x, Vector3.Distance(transform.position, door.position));
        }
    }

    public void SetWidth(float width) => spriteRenderer.size = new Vector2(width, spriteRenderer.size.y);
}
