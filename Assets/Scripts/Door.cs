using System;
using UnityEngine;

public class Door : MonoBehaviour
{
    public Action OnSphereEnterDoor;

    public bool IsOpen { get; private set; }

    private Animator animator;
    private float    sphereLayer;

    void Awake()
    {
        animator = GetComponent<Animator>();
        sphereLayer = LayerMask.NameToLayer("Sphere");
    }

    public void Open() => animator.SetBool("isOpen", IsOpen = true);
    
    public void Close() =>animator.SetBool("isOpen", IsOpen = false);

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.layer == sphereLayer)
        {
            Close();
            OnSphereEnterDoor?.Invoke();
        }
    }
}
