using System;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerSphere : MonoBehaviour
{
    public Action OnSphereHitObstacle;
    
    [SerializeField] private Transform startPosition;
    [SerializeField] private float     forceModifier;
    [SerializeField] private float     explosionForce;
    [SerializeField] private float     impactRadius;

    private Renderer  renderer;
    private Rigidbody rigidbody;
    private int       obstacleLayer;
    private float     initialImpactRadius;
    
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        renderer = GetComponent<Renderer>();
        obstacleLayer = LayerMask.NameToLayer("Obstacle");
        initialImpactRadius = impactRadius;
    }

    public void TakeStartingPosition() => transform.position = startPosition.position;
    
    public void TakeShot() => rigidbody.AddForce(Vector3.forward * forceModifier, ForceMode.Impulse);
    
    public void ChangeRendererEnabledState(bool state) => renderer.enabled = state;
    
    public void ChangeSize(float size)
    {
        impactRadius = initialImpactRadius * size;
        transform.localScale = new Vector3(size, size, size);
    }

    public void ResetPhysicalForces()
    {
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.gameObject.layer == obstacleLayer)
        {
            Collider[] overlappedColliders = Physics.OverlapSphere(transform.position, impactRadius);
            for (int i = 0; i < overlappedColliders.Length; i++)
            {
                var obstacle = overlappedColliders[i].GetComponent<Obstacle>();
                if (obstacle != null)
                {
                   obstacle.ChangeColor(Color.red);
                   obstacle.Explode(transform.position, explosionForce, impactRadius);
                }
            }

            ResetPhysicalForces();
            OnSphereHitObstacle?.Invoke();
        }
    }
}
